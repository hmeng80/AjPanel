#!/bin/sh

#download autoupdate script
# Configuration
plugin="version"
version="1.1"
url="https://gitlab.com/hmeng80/AjPanel/-/raw/main/version-1.1.tar.gz"
targz_file="$plugin-$version.tar.gz"
temp_dir="/tmp"

#Download & install package
echo "> Downloading autoupdate script please wait ..."
sleep 3
wget -qO $temp_dir/$targz_file --no-check-certificate $url
tar -xzf $temp_dir/$targz_file -C /
extract=$?
rm -rf $temp_dir/$targz_file >/dev/null 2>&1

if [ $extract -eq 0 ]; then
echo "> autoupdate script installed successfully"
sleep 3
echo "> Uploaded By Haitham "
else
echo "> autoupdate script installation failed"
sleep 3
fi

#activate panel auto update cron time
sed -i '/\*\/3 \* \* \* \* \/usr\/script\/autoupdate-Haitham-panel.sh/d ' /etc/cron/crontabs/root > /dev/null 2>&1

if [ ! -f /etc/cron/crontabs/root ]; then
mkdir /etc/cron >/dev/null 2>&1
mkdir /etc/cron/crontabs >/dev/null 2>&1
touch /etc/cron/crontabs/root 
fi
echo "> activating panel autoupdate please wait ..."
sleep 3s

echo "03 * * * * /usr/script/autoupdate-Haitham-panel.sh" >> /etc/cron/crontabs/root

# Restart Enigma2 service or kill
echo "> your device will restart now please wait ..."
if command -v dpkg &> /dev/null; then
    sleep 2
    systemctl restart enigma2
else
    sleep 2
    killall -9 enigma2
fi