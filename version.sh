#!/bin/sh

lpv=$(sed -n '/About/p' /media/hdd/AJPanel_Backup/ajpanel_menu_Haitham.xml | sed 's/^.*">/">/' | sed 's/">*//' | sed 's/<.*//') 
echo "local panel version: $lpv"

sleep 3
wpv=$(wget -qO  /tmp/version.txt --no-check-certificate https://gitlab.com/hmeng80/AjPanel/-/raw/main/version.txt | sed -n '/20/p' /tmp/version.txt)
echo "web   panel version: $wpv"

if [ $lpv == $wpv ]; then
echo "> panel is uptodate"
else 
echo "> need to uptodate panel"
wget -q "--no-check-certificate" https://gitlab.com/hmeng80/AjPanel/-/raw/main/update.sh -O - | /bin/sh
fi
